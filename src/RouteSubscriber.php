<?php

namespace Drupal\og_members_tab;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * RouteSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(\Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Remove the group actions route.
   */
  protected function alterRoutes(RouteCollection $collection) {
    // @see \Drupal\og\Routing\RouteSubscriber::alterRoutes
    // @see \Drupal\og\EventSubscriber\OgEventSubscriber::provideOgAdminRoutes
    foreach ($this->entityTypeManager->getDefinitions() as $entityTypeId => $entityType) {
      if (!$entityType->hasLinkTemplate('canonical')) {
        // Entity type doesn't have a canonical route.
        continue;
      }

      if (!$og_admin_path = $entityType->getLinkTemplate('og-admin-routes')) {
        // Entity type doesn't have the link template defined.
        continue;
      }

      $entityTypeId = $entityType->id();
      $adminRouteName = "entity.$entityTypeId.og_admin_routes";
      $collection->remove($adminRouteName);
    }
  }

}
